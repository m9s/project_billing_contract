# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Project Billing Contract',
    'name_de_DE': 'Projektverwaltung Abrechnung mit Verträgen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides integration for billing of projects with contracts
    ''',
    'description_de_DE': '''
    - Integriert die Abrechnung von Projekten mit Verträgen.
    ''',
    'depends': [
        'contract_billing_pricelist',
        'project_billing',
        'timesheet_billing_contract',
        'timesheet_billing_pricelist'
    ],
    'xml': [
        'contract.xml',
        'project.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}

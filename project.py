# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Equal, If, Bool


class Work(ModelSQL, ModelView):
    _name = 'project.work'

    contract = fields.Many2One('contract.contract', 'Contract', context={
                'party': Eval('party')
            }, domain=['OR',
                ('party_invoice',
                    If(Equal(Eval('type'), 'project'), '=', '!='),
                    Eval('party')),
                ('party_service',
                    If(Equal(Eval('type'), 'project'), '=', '!='),
                    Eval('party')),
            ], states={
                'invisible': Not(Equal(Eval('type'), 'project')),
                'required': Equal(Eval('type'), 'project'),
                'readonly': Bool(Eval('timesheet_lines'))
            }, depends=['type', 'party', 'timesheet_lines'])

    def get_invoicing_party(self, project):
        if project.type == 'task':
            project = project.parent
        return project.contract.party_invoice

    def get_contract(self, project):
        if project.type == 'task':
            project = project.parent
        return project.contract

    def get_pricelist(self, project):
        if isinstance(project, (int, long)):
            project = self.browse(project)
        if project.type == 'task':
            project = project.parent
        if project.contract.pricelist:
            res = project.contract.pricelist
        return res

    def _compute_billing_line_vals(self, line, defaults):
        res = super(Work, self)._compute_billing_line_vals(line, defaults)
        for billing_line in res:
            billing_line['contract'] = (defaults['contract'] and
                defaults['contract'].id or False)
            billing_line['state'] = 'confirmed'
        return res

Work()
